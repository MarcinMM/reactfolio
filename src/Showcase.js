import React, { Component } from "react";
import SingleItemBox from "./SingleItemBox";
import { ShowcaseItems } from "./ShowcaseItems";

class Showcase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {}
    };
  }

  componentDidMount() {
    this.setState({
      items: ShowcaseItems()
    });
  }

  render() {
    var items = [];
    this.state.items.forEach(function(item) {
      items.push(
        <SingleItemBox item={item.details} screenshots={item.screenshots} />
      );
    });
    return (
      <div className="Showcase">
        {items}
      </div>
    );
  }
}

export default Showcase;
