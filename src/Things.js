import React, { Component } from "react";

class Things extends Component {
  render() {
    return (
      <div className="Things">
        <span className="highlight">Skills:</span> AWS, React, Java, Ruby,
        Rails, Python<br />
        <span className="highlight">More obvious skills:</span> CI, TDD, Git,
        Jenkins, shell scripting, OOP, MVC, FP, SQL, HTML, CSS, SVN<br />
        <span className="highlight">Skills partly forgotten:</span> C#, AS3,
        PHP, Oracle Applications
      </div>
    );
  }
}

export default Things;
