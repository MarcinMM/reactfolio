export function ShowcaseItems() {
  return {
    items: {
      quango: {
        screenshots: {
          names: {
            "1": "Sortable product overview",
            "2": "Multi-tabbed form entry.",
            "3": "Document association page.",
            "4": "Resulting dynamic product page."
          },
          length: "4",
          descriptions: {
            "1":
              "This implements the Yii data manager with live search and sort. Appearance was themed with a third-party downloadable theme that needed to be integrated with Yii defaults. Theme style included inputs, navigation, overall look and feel of page, AJAX prompts, etc.",
            "2":
              "I had to tweak Yii to allow me to include multiple models (product, photos, documents) onto one 'view', which took a little longer than expected. Still, I was pleased with the results: top tabs determine type of product details to be edited, side tabs switch between product info panels.",
            "3":
              "This page lets you attach documentation to products. The only tricky parts were duplicate attachment prevention, a check for unique and active documents, and preventing the user from trying to add/remove without selecting anything. The lower half includes a live search - just start typing to get a list of available documents.",
            "4":
              "This view reads the product id, loads appropriate info, fetches associated documents and images and creates a product overview page with downloads and a slideshow."
          }
        },
        details: {
          name: "Ametek Product Manager",
          skills: "PHP, jQuery, Yii, CSS, mySQL",
          image: "assets/images/quango1/quango1.jpg",
          description:
            "A product manager system completed using an existing framework (Yii) and a downloadable theme (Adminus). The challenge was to write less code and use existing technology to complete the task within 2 weeks. I have not used Yii before, but its code generation tools and flexible association model fit the need perfectly. Completion took 9 days."
        }
      },
      ddweller: {
        screenshots: {
          names: {
            "1": "A typical level"
          },
          length: "1",
          descriptions: {
            "1":
              "A sample level. All rooms, corridors, items and player are placed dynamically on each level. All code and placeholder item art by me; all pixel art by ADC. Note that image shows pre-alpha debug information."
          }
        },
        details: {
          name: "Dungeon Dweller (WIP)",
          skills: "AS3",
          image: "assets/images/ddweller/ddweller.png",
          description:
            "A roguelike written using Flashpunk and AS3 in FlashDevelop. It currently is able to generate dynamic levels on the fly; generate pathways using A* pathfinding, and generate random items and place them using Flashpunk's collision detection. Player movement and item detection is in as well; next up is player inventory. Pixel art by ADC; placeholder art mine."
        }
      },
      dnet: {
        screenshots: {
          names: {
            "1": "Admin vs. live view",
            "2": "Widget admin",
            "3": "Admin view with content.",
            "4": "Live view with content."
          },
          length: "4",
          descriptions: {
            "1":
              "Admin view on top, and actual page layout below. Note widget dropdowns - new widgets will appear in those respective columns. Note widget variety in dropdown.",
            "2":
              "Modular widget control. Each widget contains everything it needs to perform its own administration and rendering. This lets the developer(s) simply drop in new widgets without worrying about dependencies (of course it means a lot of redundant code).",
            "3":
              "Admin after mere moments of adding widgets. It was intended a user is up and running almost immediately with the basic widgets.",
            "4":
              "Live view after enabling widgets; widgets start out in disabled state so that they can be configured. A single click enables them for use."
          }
        },
        details: {
          name: "D.net CMS (~2000)",
          skills: "PHP, mySQL, jQuery",
          image: "assets/images/dnet/dnet.png",
          description:
            "A CMS focused on getting the user up and running in minutes. Packaged			with a quick install process and using a WYSIWYG interface in an era of			obtuse dropdowns and configuration checkboxes, D.net let you edit, add, remove and move widgets from where			they were on the page with single button clicks.			<b>Note: D.Net used to have a mySQL injection vulnerability which has been fixed.</b>"
        }
      },
      wt: {
        screenshots: {
          names: {
            "1": "Initial admin page",
            "2": "News admin",
            "3": "Article management",
            "4": "Editors' page with widgets",
            "5": "Site home with reduced widget view"
          },
          length: "5",
          descriptions: {
            "1":
              "Simple menu with all used functions on top. This screen gets you into the most commonly used functions, i.e. the creation of new content or editing of previous content.",
            "2":
              "Simple news entry form; username pre-populated from login credentials. UI allows for before/after the fold content, source-citing, and media uploads. Again, the most common functions are all available from one form.",
            "3":
              "This screen lets you manage not only the text content on the site but also the layout in a limited fashion. You can quickly update timestamps on content as needed, turn featured content on and off, and toggle visibility status. There's also a preview mode for proofreading by other editors.",
            "4":
              "This screen illustrates the widget-driven concept with a total of 8 widgets all tied to current editor displayed: latest content on the left, and personal stats and information on the right. In the content column the bio and mini-blog are content-managed as well.",
            "5":
              "Site homepage illustrating a reduced amount of widgets: layout can be experimented with by simply including or commenting out relevant widgets without fear of losing dependencies. "
          }
        },
        details: {
          name: "Wicked Toast (~2003)",
          skills: "PHP, mySQL",
          image: "assets/images/wt/wt.png",
          description:
            'Wicked Toast was a hobby game review site which used a quick and dirty UI to let editors and writers add and manage content quickly. 					I used the "widget" concept to quickly shift					content around as I experimented with the layout, but the widgets were 					simple file includes. The most difficult part of this					was the XML parser; I wrote my own as theSimpleXML library					did not exist yet. '
        }
      },
      somi: {
        screenshots: {
          names: {
            "1": "Embeddable view.",
            "2": "Facebook full app view."
          },
          length: "2",
          descriptions: {
            "1":
              "This widget-sized layout could be inserted into any page that allowed iframes, and the game could be played from here with the help of a crossdomain channel and some javascript events. The bottom bar housed control, sign-in and purchase popups.",
            "2":
              "On the game's homepage on Facebook, a user could browse forums and game information as well as play the game. There were also friends' presence widgets, achievement comparisons, and an embed widget URL generator."
          }
        },
        details: {
          name: "Secret of Monkey Island (Facebook) (2009)",
          skills: "PHP, jQuery, REST API, Facebook, eCommerce",
          image: "assets/images/somi/somi.png",
          description:
            "Secret of Monkey Island was our first very public third party integration, created with social features in mind. It was also the first foray into e-commerce, using our own platform and API. We switched to jQuery due to Ext.js being too heavy, but the bulk of work went into Facebook integration. "
        }
      },
      legions: {
        screenshots: {
          names: {
            "1": "Legions front page",
            "2": "Scoreboard and course selector",
            "3": "Friend and global scoreboard"
          },
          length: "3",
          descriptions: {
            "1":
              "Navigation menu with standard Facebook items. Note friend widget on bottom powered by jQuery and Facebook API.",
            "2":
              "Scoreboard with current scores and top players. Due to the complexity of this widget the routes were loaded first, and the users and scores later. This one was a team effort.",
            "3":
              "This widget grabbed your friends from Facebook and their scores from the game; its secondary function was to grab the top times as well, so it also did this process in reverse (first the top scores, then the corresponding users). It would also auto-scroll to your own score in either list."
          }
        },
        details: {
          name: "PlayLegions.com (2009)",
          image: "assets/images/legions/legions.png",
          skills: "PHP, Ext.js, REST API, Blueprint, Facebook",
          description:
            "PlayLegions was a standalone proof of concept site that enabled users to play a racing game in their browser. Users were also able to store high scores per track, and play the game on its own site as well as on Facebook with all standard Facebook functionality such as friends list, invites and challenges. Site is no longer active as the company dissolved."
        }
      },
      labs: {
        screenshots: {
          names: {
            "1": "Customer management center",
            "2": "Customer management - ownership management",
            "3": "E-commerce catalog management."
          },
          length: "3",
          descriptions: {
            "1":
              "Multi purpose panel allowing access to user parameters, a banlist, and a live feed of most recent actions taken within the system. The live feed would switch to most recent actions on specific user once user was found.",
            "2":
              "Modal view for granting products, confirmation screen. Actual UI allowed the admin to live search from a list of available products; ownership was granted in realtime.",
            "3":
              "From this screen the admin could access all items belonging to them, their properties, and their e-commerce status. You could change names, prices, or methods of purchase, and even collect items into bundles, gift packs, or promo options. Changes were packaged and deployed as a unit pending workflow review."
          }
        },
        details: {
          name: "InstantAction Labs (2010)",
          skills: "PHP, jQuery, REST API, mySQL",
          image: "assets/images/labs/labs.png",
          description:
            "IA Labs was a multi-client content management system intended as a complete solution for managing a next generation game network website. A developer or publisher would be able to log in, manage, preview and publish their content. The system also included user lookup and management; hierarchical permissions and game management were planned as well."
        }
      }
    }
  };
}
