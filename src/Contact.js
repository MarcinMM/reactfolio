import React, { Component } from "react";

class Contact extends Component {
  render() {
    return (
      <div className="Contact">
        <span className="highlight">Contact:</span> marcin@codesquares.net<br />
        <span className="highlight">Places:</span>
        <a href="https://www.linkedin.com/in/marcinmanek/">LinkedIn</a>
      </div>
    );
  }
}

export default Contact;
